import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'speedUnit'
})

export class SpeedUnitPipe implements PipeTransform {
    transform(speed: number, unitType: string) {
        switch(unitType){
            case "kph":
            const kilometers = speed * 1.62;
            return Number(kilometers).toFixed(0) + " "+"kph";
            default: 
            return Number(speed).toFixed(0) + " "+"mph";
        }
    }
}